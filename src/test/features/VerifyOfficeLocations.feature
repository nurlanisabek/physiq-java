@officeLocationVerification
Feature: Verify the Main and Naperville office location are correct

  Scenario: Verify the Main office locations
    Given user in contact page
    Then user sees the "MAIN OFFICE" address
      | 200 W. Jackson Blvd |
      | Suite 550           |
      | Chicago, Illinois   |
      | 60606               |
      | United States       |

  Scenario: Verify the Naperville office locations
    And user in contact page
    Then user sees the "NAPERVILLE OFFICE" address
      | 300 E. 5th Avenue    |
      | Suite 105            |
      | Naperville, Illinois |
      | 60563                |
      | United States        |