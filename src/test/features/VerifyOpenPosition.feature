@verifyOpenPosition
Feature: Verify open position in openings

  @verifyOpeningByName
  Scenario: Verify open position in openings
    Given user in Join Our Team page
    Then user verifies open position with name "Inventory Specialist"

  @verifyDetailsOfThePage
  Scenario: Take a screenshot of the position Inventory Specialist
    Given user in Join Our Team page
    Then user clicks Inventory Specialist positions Read More button and takes a screenshot