@fillOutContactForm
Feature: Fill out contact form and on the Contact page

  Scenario: Fill out contact form and take a screen shot of the filled form
    Given user in contact page
    Then user fills out contact page with firstName "James", lastName "Bond" email "jamesBond@mail.com", title "coordinator of the special forces", institution "MI 6", phoneNumber "555-555-5555" and takes a screen shot