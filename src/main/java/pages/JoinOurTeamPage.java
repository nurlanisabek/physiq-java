package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

public class JoinOurTeamPage {
    public JoinOurTeamPage() {
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(xpath = "//section[@id='main-section']")
    public WebElement openings;

    //a[contains(@href,'inventory-specialist') and text()='READ MORE']
    @FindBy(xpath = "//a[contains(@href,'inventory-specialist') and text()='READ MORE']")
    public WebElement positionInventorySpecialistDetailsButton;


    @FindBy(xpath = "//h2[text()='Job Description']")
    public WebElement jobDiscription;
}
