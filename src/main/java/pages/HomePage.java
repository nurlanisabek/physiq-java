package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

public class HomePage {

    public HomePage() {
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(id = "navContactUs")
    public WebElement contactButton;

    @FindBy(id = "navJoinOurTeam")
    public WebElement joinOurTeam;


    @FindBy(xpath = "//div/button[text()='ACCEPT']")
    public WebElement acceptCookies;
}
