package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

public class ContactPage {

    public ContactPage() {
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(xpath = "//iframe[starts-with(@id,'ssf_SzFLM')]")
    public WebElement contactFormIframe;

    @FindBy(xpath = "//input[@default-placeholder='First Name']")
    public WebElement firstNameField;

    @FindBy(id = "397114370")
    public WebElement lastNameField;

    @FindBy(id = "397115394")
    public WebElement emailField;

    @FindBy(id = "397117442")
    public WebElement titleField;

    @FindBy(id = "397116418")
    public WebElement institutionNameField;

    @FindBy(id = "397120514")
    public WebElement phoneNumberField;

    @FindBy(xpath = "//h3[text()='MAIN OFFICE']/..")
    public WebElement officeLocation;
}
