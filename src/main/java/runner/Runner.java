package runner;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumberReports", "json:target/report.json"},
        features = "src/test/features",
        glue = "steps",
//        tags = "@verifyDetailsOfThePage",   // if user wants runable by tags
        dryRun = false
)
public class Runner {
}
