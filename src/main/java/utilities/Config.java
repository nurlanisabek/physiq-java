package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

public class Config {

    private static Properties properties = new Properties();

    static {
        String path = "configuration.properties";
        try {
            FileInputStream file = new FileInputStream(path);
            properties.load(file);
            file.close();
        } catch (IOException e) {
            Config.LOGGER.warning("Properties file not found");
        }
    }

    public static String getProperty(String keyword) {
        return properties.getProperty(keyword);
    }

    public static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
}
