package steps;

import cucumber.api.java.en.Then;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pages.ContactPage;
import utilities.Driver;

import java.io.File;
import java.io.IOException;

public class FillOutContactFormAndTakeScreenShotSteps {

    /**
     * function is for filling up the contact information and taking screenshot after
     *
     * @param firstName   String
     * @param lastName    String
     * @param email       String
     * @param title       String
     * @param institution String
     * @param phoneNumber String
     */
    @Then("user fills out contact page with firstName {string}, lastName {string} email {string}, title {string}, institution {string}, phoneNumber {string} and takes a screen shot")
    public void user_fills_out_contact_page_with_firstName_lastName_email_title_institution_phoneNumber_and_takes_a_screen_shot(String firstName, String lastName, String email, String title, String institution, String phoneNumber) {

        Driver.getDriver().switchTo().parentFrame();
        Driver.getDriver().switchTo().frame(0);

        ContactPage contactPage = new ContactPage();

        Actions actions = new Actions(Driver.getDriver());
        actions.moveToElement(contactPage.firstNameField).perform();

        contactPage.firstNameField.sendKeys(firstName);
        contactPage.lastNameField.sendKeys(lastName);
        contactPage.emailField.sendKeys(email);
        contactPage.titleField.sendKeys(title);
        contactPage.institutionNameField.sendKeys(institution);
        contactPage.phoneNumberField.sendKeys(phoneNumber);

        //Take the screenshot
        File screenshot = ((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.FILE);

        //Copy the file to a location and use try catch block to handle exception
        try {
            FileUtils.copyFile(screenshot, new File("target/cucumberReports//filledForm.png"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}