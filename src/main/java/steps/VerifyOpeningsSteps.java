package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.HomePage;
import pages.JoinOurTeamPage;
import utilities.Config;
import utilities.Driver;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class VerifyOpeningsSteps {

    JoinOurTeamPage joinOurTeamPage = new JoinOurTeamPage();

    /**
     * The method for landing to Join Our Page page
     */
    @Given("user in Join Our Team page")
    public void user_in_join_our_team_page() {
        HomePage homePage = new HomePage();
        Driver.getDriver().get(Config.getProperty("url"));
        homePage.acceptCookies.click();
        homePage.joinOurTeam.click();
    }

    /**
     * The function verifies opening position given data
     *
     * @param openingPosition that method looks to find from opening page
     */
    @Then("user verifies open position with name {string}")
    public void user_verifies_open_position_with_name(String openingPosition) {

        WebElement openings = joinOurTeamPage.openings;
        Assert.assertTrue("Missing open position with name: " + openingPosition, openings.getText().contains(openingPosition));
    }

    /**
     * function is for taking screenshot of Inventory Specialist's job description
     */
    @Then("user clicks Inventory Specialist positions Read More button and takes a screenshot")
    public void user_clicks_inventory_specialist_more_details_and_takes_a_screenshot() {

        JoinOurTeamPage joinOurTeamPage = new JoinOurTeamPage();
        joinOurTeamPage.positionInventorySpecialistDetailsButton.click();

        Actions action = new Actions(Driver.getDriver());
        action.moveToElement(joinOurTeamPage.jobDiscription).perform();

        //Take the screenshot
        File screenshot = ((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.FILE);

        //Copy the file to a location and use try catch block to handle exception
        try {
            FileUtils.copyFile(screenshot, new File("target/cucumberReports//InventorySpecialist.png"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}