package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pages.ContactPage;
import pages.HomePage;
import utilities.Config;
import utilities.Driver;

import java.util.ArrayList;
import java.util.List;

public class OfficeLocationSteps {

    /**
     * User verifies locations each field
     *
     * @param officeName name of the office where address field needs to get validated
     * @param dataTable  data table stores office locations full address
     */
    @Then("user sees the {string} address")
    public void user_sees_the_address(String officeName, io.cucumber.datatable.DataTable dataTable) {

        List<String> listOfElements = dataTable.asList();
        List<String> listOfMissingElements = new ArrayList<String>();
        ContactPage contactPage = new ContactPage();
        WebElement officeAddress = contactPage.officeLocation;

        // action for scrolling to office element. If scenario fails, hooks can take a picture as well
        Actions actions = new Actions(Driver.getDriver());
        actions.moveToElement(officeAddress).perform();

        for (String element : listOfElements) {
            if (!(officeAddress.getText().contains(element)))
                listOfMissingElements.add(element);
        }
        Assert.assertTrue(officeName + " location address is wrong. Missing or spelled incorrectly address field: " + listOfMissingElements, listOfMissingElements.isEmpty());
    }

    /**
     * Method for landing to contact page
     */
    @Given("user in contact page")
    public void user_in_contact_page() {
        HomePage homePage = new HomePage();
        Driver.getDriver().get(Config.getProperty("url"));
        homePage.contactButton.click();
    }
}