package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import utilities.Config;
import utilities.Driver;

public class Hooks {

    @After
    public void tearDown(Scenario scenario) {

        Config.LOGGER.info(scenario.getName());
        Config.LOGGER.warning("Scenario is: " + scenario.getStatus().toString());

        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) Driver.getDriver())
                    .getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png"); //stick it in the report
        }
        Driver.quitDriver();
    }
}