$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/features/FillOutContactForm.feature");
formatter.feature({
  "name": "Fill out contact form and on the Contact page",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@fillOutContactForm"
    }
  ]
});
formatter.scenario({
  "name": "Fill out contact form and take a screen shot of the filled form",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@fillOutContactForm"
    }
  ]
});
formatter.step({
  "name": "user in contact page",
  "keyword": "Given "
});
formatter.match({
  "location": "OfficeLocationSteps.user_in_contact_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user fills out contact page with firstName \"James\", lastName \"Bond\" email \"jamesBond@mail.com\", title \"coordinator of the special forces\", institution \"MI 6\", phoneNumber \"555-555-5555\" and takes a screen shot",
  "keyword": "Then "
});
formatter.match({
  "location": "FillOutContactFormAndTakeScreenShotSteps.user_fills_out_contact_page_with_firstName_lastName_email_title_institution_phoneNumber_and_takes_a_screen_shot(String,String,String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("src/test/features/VerifyOfficeLocations.feature");
formatter.feature({
  "name": "Verify the Main and Naperville office location are correct",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@officeLocationVerification"
    }
  ]
});
formatter.scenario({
  "name": "Verify the Main office locations",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@officeLocationVerification"
    }
  ]
});
formatter.step({
  "name": "user in contact page",
  "keyword": "Given "
});
formatter.match({
  "location": "OfficeLocationSteps.user_in_contact_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user sees the \"MAIN OFFICE\" address",
  "rows": [
    {
      "cells": [
        "200 W. Jackson Blvd"
      ]
    },
    {
      "cells": [
        "Suite 550"
      ]
    },
    {
      "cells": [
        "Chicago, Illinois"
      ]
    },
    {
      "cells": [
        "60606"
      ]
    },
    {
      "cells": [
        "United States"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "OfficeLocationSteps.user_sees_the_address(String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify the Naperville office locations",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@officeLocationVerification"
    }
  ]
});
formatter.step({
  "name": "user in contact page",
  "keyword": "And "
});
formatter.match({
  "location": "OfficeLocationSteps.user_in_contact_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user sees the \"NAPERVILLE OFFICE\" address",
  "rows": [
    {
      "cells": [
        "300 E. 5th Avenue"
      ]
    },
    {
      "cells": [
        "Suite 105"
      ]
    },
    {
      "cells": [
        "Naperville, Illinois"
      ]
    },
    {
      "cells": [
        "60563"
      ]
    },
    {
      "cells": [
        "United States"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "OfficeLocationSteps.user_sees_the_address(String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("src/test/features/VerifyOpenPosition.feature");
formatter.feature({
  "name": "Verify open position in openings",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@verifyOpenPosition"
    }
  ]
});
formatter.scenario({
  "name": "Verify open position in openings",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@verifyOpenPosition"
    },
    {
      "name": "@verifyOpeningByName"
    }
  ]
});
formatter.step({
  "name": "user in Join Our Team page",
  "keyword": "Given "
});
formatter.match({
  "location": "VerifyOpeningsSteps.user_in_join_our_team_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user verifies open position with name \"Inventory Specialist\"",
  "keyword": "Then "
});
formatter.match({
  "location": "VerifyOpeningsSteps.user_verifies_open_position_with_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Take a screenshot of the position Inventory Specialist",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@verifyOpenPosition"
    },
    {
      "name": "@verifyDetailsOfThePage"
    }
  ]
});
formatter.step({
  "name": "user in Join Our Team page",
  "keyword": "Given "
});
formatter.match({
  "location": "VerifyOpeningsSteps.user_in_join_our_team_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks \"Inventory Specialist\" \"Read More\" opening and takes a screenshot",
  "keyword": "Then "
});
formatter.match({
  "location": "VerifyOpeningsSteps.user_clicks_opening_and_takes_a_screenshot(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});